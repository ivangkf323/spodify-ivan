import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  URL_API = environment.url;

  constructor(private http: HttpClient) { 
    
  }

      getDataPotify(){
        return  this.getQuery('browse/new-releases').pipe(map( data => data['albums'].items));
      }


      getArtista(termino){
          return this.getQuery(`search?q=${termino}&type=artist&limit=20`).pipe(map( data => data['artists'].items));
      }

      getArtistaIndividual(id: string){
         return this.getQuery(`artists/${id}`)/* .pipe(map( data => data)) */;
      }

      getTopTracks(id: string){
        return this.getQuery(`artists/${id}/top-tracks?country=ES`).pipe(map( data => data['tracks']));
      }

      getQuery(query: string){
        const headers = new HttpHeaders({
          'Authorization': 'Bearer BQBogtgUFHZRvalN7FJPX7DgnnI_Ud6qrUZSjuvnrj-ORqWWzPBjl7fkJ-FtPbMIuXUk9Z04fE0FpTREdvM',
        });
        return this.http.get(this.URL_API+query, {headers});
       
        
      }

      getIntervalFuntio(){
        const headers:any= new HttpHeaders({
          'grant_type': 'client_credentials',
          'client_id': 'bf051016666147b0a81687cfaf94424c',
          'client_secret': '90a93b59b08b4cbba31312e0cfd2674b'
        });

        return this.http.get('https://accounts.spotify.com/api/token', {headers});
      }
      

  }
