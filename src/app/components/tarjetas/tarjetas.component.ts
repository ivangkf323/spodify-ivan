import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styles: []
})
export class TarjetasComponent  {

  @Input() dataExterna: any []=[];
  idArtista:any;

  constructor(private router: Router) { }


  buscarArtista(item:any){
    //console.log(item);
    if(item.type === "artist"){
        this.idArtista=item.id;
    }else{
      this.idArtista=item.artists[0].id;
    }
    this.router.navigate(['/artist',this.idArtista]);
  }


}
