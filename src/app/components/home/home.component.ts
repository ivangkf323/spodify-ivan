import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  canciones: any[]=[];
  bandera:any=true;
  mensajeError:string;
  error=false;

  constructor(private spotify: SpotifyService) { 
    this.spotify.getDataPotify().subscribe( (data: any) => {
      this.canciones=data;
      //console.log(data);
      this.bandera=false;
    }, (errorServicio)=> {
      this.mensajeError=errorServicio.error.error.message;
      this.bandera=false;
      this.error=true;
      //console.log(errorServicio.error.error.message);
    });

  }

  ngOnInit() {
  }

}
