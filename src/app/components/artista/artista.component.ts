import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { SpotifyService } from 'src/app/services/spotify.service';


@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent  {
  dataid:any;
  dataArtista:any={};
  bandera:any=true;
  dataTopTracks:any[]=[];

  constructor(private activateRoter: ActivatedRoute, private servicio: SpotifyService) { 
    this.activateRoter.params.subscribe(data => {
      this.dataid=data['id'];
      console.log(data);
      this.servicio.getArtistaIndividual(this.dataid).subscribe((data:any) => {
        console.log(data);
       this.dataArtista=data;
       this.bandera=false;
      });
      this.getTopTracksArtista(data['id']);
    });

    
  }

  getTopTracksArtista(id:string){
   this.servicio.getTopTracks(id).subscribe((data:any) => {
     //console.log(data);
     this.dataTopTracks=data;
   });
  }

  

}
