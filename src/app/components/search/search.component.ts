import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  artistas:any[]=[];
  bandera:any=false;

  constructor(private spotify: SpotifyService) { }

  buscar(cancion: string){
    this.bandera=true;
    this.spotify.getArtista(cancion).subscribe( (data: any) => {
      this.artistas=data;
      this.bandera=false;
    });
  }

}
